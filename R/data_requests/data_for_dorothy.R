# Data export for Dorothy
# Preparing data for Dorothy's analysis of urine samples vs thyroxin
# M. Rolland
# 22/01/2019

# empty workspace
rm(list = ls())

# libraries
library(tidyverse)
library(labelled)
library(foreign)
library(sepages)
library(haven)
select <- dplyr::select

# load all sepage data produced by M. Rolland (to date of jan 25, 2019)
# see M. Rolland Sepages data preparation process for more info
load("data/data_clean/sepages_data.Rdata")
bdd_grossesse <- read_stata("data/data_raw/bdd_grossesse_v3.dta")

# select pregnancy samples: Trimester 1 (T1) and Trimester 3 (T3)
sepages_preg_data <- filter(samples_full, period %in% c("T1", "T3"))

# select requested variables from the samples dataset
sepages_preg_data <- select(sepages_preg_data, 
                            ident, 
                            period, 
                            INSERM_ID, 
                            MEPA_total,
                            ETPA_total,
                            PRPA_total,
                            BUPA_total,
                            BPA_total,
                            BPS_total,
                            BPF_total,
                            BPB_total,
                            BPAF_total,
                            OXBE_total,
                            TRCS_total,
                            TRCB_total,
                            MEPA_total_string,
                            ETPA_total_string,
                            PRPA_total_string,
                            BUPA_total_string,
                            BPA_total_string,
                            BPS_total_string,
                            BPF_total_string,
                            BPB_total_string,
                            BPAF_total_string,
                            OXBE_total_string,
                            TRCS_total_string,
                            TRCB_total_string,
                            MEP,
                            MiBP,
                            MnBP,
                            MBzP,
                            MEHP,
                            MEHHP,
                            MEOHP,
                            MECPP,
                            MMCHP,
                            ohMiNP,
                            oxoMiNP,
                            cxMiNP,
                            ohMINCH,
                            oxoMINCH,
                            ohMPHP,
                            MEP_string,
                            MiBP_string,
                            MnBP_string,
                            MBzP_string,
                            MEHP_string,
                            MEHHP_string,
                            MEOHP_string,
                            MECPP_string,
                            MMCHP_string,
                            ohMiNP_string,
                            oxoMiNP_string,
                            cxMiNP_string,
                            ohMINCH_string,
                            oxoMINCH_string,
                            ohMPHP_string,
                            date_pose_dom,
                            ident,
                            sample_gravity) %>%
  rename(sample_date = date_pose_dom)

# set in the one line per woman format with "_t1" or "_t3" suffix
sepages_preg_data <- add_col_strata2(sepages_preg_data, "ident", "period")

# add questionnaire vars
quest_data <- select(questionnaire,
                     ident,
                     child_sex,
                     parity,
                     doc,
                     birth_date,
                     birth_date_mom,
                     gestational_age_t1,
                     gestational_age_t3,
                     mom_age_at_sample_t1,
                     mom_age_at_sample_t3,
                     mom_height,
                     mom_weight,
                     mom_bmi,
                     mom_edu,
                     mom_ethnicity,
                     marital_status,
                     smoke_n_cig_t1,
                     smoke_n_cig_t3) %>%
  rename(birth_date_child = birth_date,
         date_of_conception = doc)

# harmonise key format and join datasets
sepages_preg_data$ident <- as.character(sepages_preg_data$ident)
quest_data$ident <- as.character(quest_data$ident)
sepages_preg_data <- left_join(sepages_preg_data, quest_data, by = "ident")

# add extra vars (that I didn't use for my analyses)
# Rq1: I don't know the coding for these variables
# Rq2: numbering weird for thyroid vars 
extra_quest <- select(bdd_grossesse,
                      ident,
                      cy1hao1_q02, # birth time
                      cy1hao1_q03, # 03. Lieu de naissance (nom établissement) 
                      mt1haa1_q07, # 07. Trouble de la glande thyroïdienne 
                      mt1haa1_q07p1a, # 07P1a. Précisez 
                      mt1haa1_q07p2a, # 07P2a. Quel(s) traitement(s) avez-vous pris pour ce problème de santé 
                      mt1haa1_q07p1) # 07P1. Age trouble thyroïde :

# harmonise key format and join datasets
extra_quest$ident <- as.character(extra_quest$ident)
sepages_preg_data <- left_join(sepages_preg_data, extra_quest, by = "ident")

# replace all empty strings by NA as stata does not accept them
for(i in 1:ncol(sepages_preg_data)){
  if(class(sepages_preg_data[[i]]) == "character"){
    sepages_preg_data[[i]][sepages_preg_data[[i]] == ""] <- NA
  }
}

# Export as .dta for Dorothy to analyse, and to preserve label
write.dta(sepages_preg_data, "data/data_clean/sepages_data_for_dorothy.dta")
