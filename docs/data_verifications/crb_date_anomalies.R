# CRB DATE ANOMALIES
# Flag crb samples where transport time or defreeze time are anomalous (too long or negative) or missing for T1 and T3
# 22/11/2018 
# M. Rolland

library(tidyverse)
library(xlsx)

# Empty workspace ----
rm(list = ls())

# Import data ----
load(file = "data/data_preparation/sepages_data_step3.Rdata")

# Flag anomalies ----
# Transport times negative or greater than 3 hours or missing for T1 and T3
transport_outliers <- crb %>%
  filter(transport_time < 0 | transport_time > 180 | is.na(transport_time)) %>%
  filter(period %in% c("T1", "T3")) %>%
  select(INSERM_ID, transport_start, transport_end, transport_time)

# Defreeze times negative or greater than 30 hours or missing for T1 and T3
defreeze_outliers <- crb %>%
  filter(defreeze_time < 0 | defreeze_time > 1800 | is.na(defreeze_time)) %>%
  filter(period %in% c("T1", "T3")) %>%
  select(INSERM_ID, defreeze_start, defreeze_end, defreeze_time)

# Export lists ----
write.xlsx(transport_outliers, "data/data_clean/transport_outliers.xlsx")
write.xlsx(defreeze_outliers, "data/data_clean/defreeze_outliers.xlsx")
