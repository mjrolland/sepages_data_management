# Sepages data management git repo

This is the git repo for the sepages data management process. It includes the production of several datasets including **COMPLETE**

## 1. Sepages pipeline

Explain briefly + add link to detailed document

For now only done on pregnancy data

## 2. Input/raw data

All data sets used in the sepages pipeline to create the datasets presented in the next chapter.

**phthalates_phenol_Sek_1_29_long20190222.csv**: NIPH data set pre-processed by C. Philippat  
**compound_list.csv**: characteristic of the measured compounds (LOD, LOQ, long name, short name, etc) provided by NIPH  
**bdd_grossesse_v3.dta**: sepages pregnancy data, produced by K. Supernant  
**demande_181005.dta**: post-nat data not yet in a fixed data set, produced by K. Supernant  
**base_crb_urine_2019.03.13.dta**: CRB samples data-set, sample protocol characteristics: transport times, defreeze times, SG, etc, produced by S. Lyon-Caen  
**donnees_echo_saisies_quest_mt1caj1.dta**: manually entered ultrasound data, provided by K. Supernant  
**demande_181205.dta**: catch-up data (if some questions of interest are missing, the question is asked again later), produced by K. Supernant  

## 3. Produced data

[date] for each **dataset_[date].csv** corresponds to dataset date of creation. Latest version of the data is stored outside of the process and locally (not online). Previous versions are stored in the `archive/` folder.

**samples_[date].csv**

* `ident` = individual identifier
* `INSERM_ID` = sample identifier
* `period` = sample period (first trimester (T1), third trimester (T3), second month (M2), first year (Y1))
* `compound` = raw value for compound
* `compound_string` = actual data returned by the NIPH before pre-process from C. Philippat
* `compound_bc` = box-cox transformed value for compound
* `compound_bc_i` = box-cox transformed value for compound + fill-ins
* `compound_bc_sd_i` = box-cox transformed value for compound + fill-ins + protocol variable correction
* `compound_i` = raw value + fill-in (box-cox back transformed from `compound_bc_i`) 
* `compound_sd_i` = raw value + fill-in + protocol variable correction (box-cox back transformed from `compound_bc_sd_i`) 
* `compound_sd_sg_i` = raw value + fill-in + protocol variable correction + sg correction (box-cox back transformed from `compound_bc_sd_sg_i`) 
* `compound_cat` = categorisation in three groups (below LOD (1), between LOD and LOQ (2) above LOQ (3)) for compounds with more than 30% data below LOQ

**samples_long_[date].csv**

add vars

**crb_[date].csv**

add vars

**ultrasound_[date].csv**

add vars

## 4. Code flow

## 5. Folders and files 

TO DO:

* list different tables
* add variable doictionnary (in readme)
* write readme
* show flow
* write pipeline document using the markdowns explaining box-cox transform, fill-in and protocol variable correction
* 